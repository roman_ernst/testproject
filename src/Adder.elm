module Adder exposing (add1)

{-| This is test module

@docs add1

-}

{-| Add one to number
-}
add1 : Int -> Int
add1 a =
    a + 1
